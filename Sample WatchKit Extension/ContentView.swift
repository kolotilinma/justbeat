//
//  ContentView.swift
//  Sample WatchKit Extension
//
//  Created by Alexey Antonov on 08/01/2020.
//  Copyright © 2020 Alexey Antonov. All rights reserved.
//

import SwiftUI
import HealthKit

struct ContentView: View {
    static let data = SongData()
    
    var body: some View {
        VStack {
            SongInfo().environmentObject(ContentView.self.data)
            Button(action: {
                ContentView.data.songFor(bpm: ContentView.self.data.currentHeartRate )
                ContentView.data.getHeartRate()
                //ContentView.data.getHeartRate()
            }) {
                Text("OK")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
