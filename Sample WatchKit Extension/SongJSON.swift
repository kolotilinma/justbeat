//
//  SongJSON.swift
//  Sample WatchKit Extension
//
//  Created by Alexey Antonov on 21/01/2020.
//  Copyright © 2020 Alexey Antonov. All rights reserved.
//

import Foundation

struct SongJSON: Codable {
    var total: Int
    var data: [Song]
}

struct Song: Codable {
    var id: Int
    var preview: String
    var title: String
}
