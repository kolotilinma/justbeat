//
//  SongChecker.swift
//  Sample WatchKit Extension
//
//  Created by Alexey Antonov on 08/01/2020.
//  Copyright © 2020 Alexey Antonov. All rights reserved.
//

import Foundation
import AVFoundation
import HealthKit
import WatchKit

class SongData:ObservableObject {
    @Published var currentSong: String = ""
    @Published var currentHeartRate: Int = 0
    
    private let healthStore = HKHealthStore()
    private let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
    private let heartRateUnit = HKUnit(from: "count/min")
    
    
    private let session = AVAudioSession.sharedInstance()
    private let podcastURLstring = "https://maximum.hostingradio.ru/maximum96.aacp"
    private var player: AVPlayer?
    private var playerItem: AVPlayerItem?
    
    
    private var audioStreamIsPlaying: Bool {
        if let audioPlayer = player {
            return audioPlayer.rate > 0 // audio is playing
        }
        
        return false
    }
    
    private func configureAudioSession() {
        
        do {
            try session.setCategory(AVAudioSession.Category.playback,
                                    mode: .default,
                                    policy: .longFormAudio,
                                    options: [])
        } catch let error {
            fatalError("*** Unable to set up the audio session: \(error.localizedDescription) ***")
        }
    }
    
    public func songFor(bpm: Int) {
        let session = URLSession.shared
        let url = URL(string: "https://api.deezer.com/search?q=bpm:\(bpm)")!
        
        
        
        let task = session.dataTask(with: url) { data, response, error in
            
            if error != nil || data == nil {
                print("Client error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                return
            }
            
            do {
                let json = try JSONDecoder().decode(SongJSON.self, from: data!)
                
                print(json.data[0].preview)
                
                if self.player == nil {
                    self.configureAudioSession() // call just before playing first audio
                }
                
                self.currentSong = json.data[0].title
                
                if let url = Bundle.main.url(forResource: "cs", withExtension: "mp3") {
//                if let url = URL(string: json.data[0].preview) {
                    
                    self.playerItem = AVPlayerItem(url: url)
                    self.player = AVPlayer(playerItem: self.playerItem)
                    
                    self.session.activate(options: []) { (success, error) in
                        
                        guard error == nil else {
                            //fatalError("*** An error occurred: \(error!.localizedDescription) ***")
                            return
                        }
                        
                        if success { // false if no audio route (e.g. no headphones, user cancel, etc.)
                            if self.audioStreamIsPlaying {
                                self.player?.pause()
                            } else {
                                self.player?.play()
                                NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
                            }
                        }
                    }
                }
                
                //let songURL = Bundle.main.url(forResource: "cs", withExtension: "mp3")
                
                //                guard let url = URL(string: json.data[0].preview) else {
                //                    return
                //                }
                
                //                let asset  = AVURLAsset(url: songURL!, options: nil)
                //                let item   = AVPlayerItem(asset: asset, automaticallyLoadedAssetKeys: nil)
                //                let player = AVPlayer(playerItem: item)
                
                //player.play()
            } catch {
                print("JSON error: \(error.localizedDescription)")
            }
        }
        
        task.resume()
        
    }
    
    @objc func playerDidFinishPlaying() {
        self.getHeartRate()
        self.songFor(bpm: currentHeartRate)
    }
    
    private func createStreamingQuery() -> HKQuery {
        
        let predicate = HKQuery.predicateForSamples(withStart: NSDate() as Date, end: nil, options: [])
        
        let query = HKAnchoredObjectQuery(type: heartRateType, predicate: predicate, anchor: nil, limit: Int(HKObjectQueryNoLimit)) {
            (query, samples, deletedObjects, anchor, error) -> Void in
            self.formatSamples(samples: samples)
        }
        
        query.updateHandler = { (query, samples, deletedObjects, anchor, error) -> Void in
            print("OK")
            self.formatSamples(samples: samples)
        }
        
        return query
    }
    
    private func formatSamples(samples: [HKSample]?) {
        guard let samples = samples as? [HKQuantitySample] else { return }
        guard let quantity = samples.last?.quantity else { return }
        
        let value = Int(quantity.doubleValue(for: heartRateUnit))
        self.currentHeartRate = value
        //print("HeartRate: \(value)")
    }
    
    func getHeartRate() {
        self.healthStore.requestAuthorization(toShare: nil, read: [self.heartRateType]) { success, error in
            
            guard success else {
                print("Bad")
                return
            }
            
            self.healthStore.execute(self.createStreamingQuery())
        }
    }
}
