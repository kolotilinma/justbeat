//
//  HostingController.swift
//  Sample WatchKit Extension
//
//  Created by Alexey Antonov on 08/01/2020.
//  Copyright © 2020 Alexey Antonov. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
