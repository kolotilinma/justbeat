//
//  SongInfo.swift
//  Sample WatchKit Extension
//
//  Created by Alexey Antonov on 21/01/2020.
//  Copyright © 2020 Alexey Antonov. All rights reserved.
//

import SwiftUI

struct SongInfo: View {
    @EnvironmentObject var data: SongData
    
    var body: some View {
        VStack {
            Text("HR: \(data.currentHeartRate)")
            Text("Current song: \(data.currentSong)")
        }
    }
}

struct SongInfo_Previews: PreviewProvider {
    static var previews: some View {
        SongInfo()
    }
}
